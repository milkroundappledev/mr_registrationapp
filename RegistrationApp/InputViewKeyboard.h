//
//  InputViewKeyboard.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 28/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputViewKeyboard : UITextView <UITextViewDelegate>
{
    UIView *inputView;
   // NSString *text;
    
}

//@property (nonatomic, retain) NSString *text;

-(IBAction)takeInputFromTitle:(id)sender;
-(IBAction)doDelete:(id)sender;
@end
