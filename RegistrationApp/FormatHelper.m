//
//  FormatHelper.m
//  RegistrationApp
//
//  Created by Afzal Valiji on 04/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "FormatHelper.h"

@implementation FormatHelper


+ (NSString *) GetStringFromDate:(NSDate *) date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    return [dateFormatter stringFromDate:date];
    
}

+ (NSDate *) GetDateFromString:(NSString *) str{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    return [dateFormatter dateFromString:str];
}

+ (NSString *) GetDBStringFromDate:(NSDate *) date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:date];
    
}

@end
