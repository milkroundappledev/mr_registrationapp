//
//  RegistrationVC.m
//  RegistrationApp
//
//  Created by Afzal Valiji on 01/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "RegistrationVC.h"
#import "RegistrationHelper.h"
#import "FormatHelper.h"
//#import "FeedbackMessageOverlay.h"
#import "Constants.h"

@interface RegistrationVC ()

@end

@implementation RegistrationVC

@synthesize CourseLevelList;
@synthesize GraduationYearList;
@synthesize firstNameKeyboardView;
@synthesize lastNameKeyboardView;
@synthesize emailNameKeyboardView;
@synthesize candidateDetails;
@synthesize messageOverlay;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self GenerateGraduationYearList];
    [self GetCourseLevelListFromDB];
    
    if(candidateDetails == nil)
    {
        // creating new candidate
        self.UpdateOnly = false;
        candidateDetails = [[CandidateDetails alloc] init];
    }
    else
    {
        // updating passed in candidate
        self.UpdateOnly = true;
        for (PopOverListItem * cl in CourseLevelList) {
            if(cl.Id == candidateDetails.CourseLevelId)
            {
                [_uxCourseLevelButton setTitle:cl.Description forState:UIControlStateNormal];
                break;
            }
        }

        firstNameKeyboardView.text = candidateDetails.FirstName;
        lastNameKeyboardView.text = candidateDetails.LastName;
        emailNameKeyboardView.text = candidateDetails.Email;
        [_uxGraduationYearButton setTitle:[NSString stringWithFormat:@"%d", candidateDetails.SchoolLeavingYear] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)GenerateGraduationYearList
{
    GraduationYearList = [[NSMutableArray alloc] init];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger year = [components year];
    
    int startDate = year-30;
    int endDate = year+8;
    
    for (int loop = endDate; loop > startDate; loop--)
    {
        PopOverListItem *item = [[PopOverListItem alloc] init];
        item.Id = loop;
        NSString *description = [NSString stringWithFormat:@"%d", loop];
        [item setDescription:description];
        [GraduationYearList addObject:item];
    }
}

-(void)GetCourseLevelListFromDB
{
    RegistrationHelper * dbHelper = [[RegistrationHelper alloc] init];
    
    CourseLevelList = [[NSMutableArray alloc] init];
    
    CourseLevelList = [dbHelper getAllCourseLevelList];
}

- (IBAction)GraduationYearClicked:(id)sender
{
    if (_GraduationYearPopOver == nil) {
        _GraduationYearPopOverVC = [[PopOverGenericVC alloc] initWithArray:GraduationYearList andStyle:UITableViewStylePlain];
        
        _GraduationYearPopOverVC.popOverDelegate = self;
    }
    
    if (_GraduationYearPopOver == nil) {
        _GraduationYearPopOver = [[UIPopoverController alloc] initWithContentViewController:_GraduationYearPopOverVC];
        [_GraduationYearPopOver presentPopoverFromRect:_uxGraduationYearButton.bounds inView:_uxGraduationYearButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        [_GraduationYearPopOver dismissPopoverAnimated:YES];
        _GraduationYearPopOver = nil;
    }
}



- (IBAction)CourseLevelClicked:(id)sender
{
    if (_CourseLevelPopOver == nil) {
        _CourseLevelPopOverVC = [[PopOverGenericVC alloc] initWithArray:CourseLevelList andStyle:UITableViewStylePlain];
        
        _CourseLevelPopOverVC.popOverDelegate = self;
    }
    
    if (_CourseLevelPopOver == nil) {
        _CourseLevelPopOver = [[UIPopoverController alloc] initWithContentViewController:_CourseLevelPopOverVC];
        [_CourseLevelPopOver presentPopoverFromRect:_uxCourseLevelButton.bounds inView:_uxGraduationYearButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        [_CourseLevelPopOver dismissPopoverAnimated:YES];
        _CourseLevelPopOver = nil;
    }
}

- (IBAction) CancelButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void)selectedItem:(PopOverListItem *)newItem
{
    if(_GraduationYearPopOver)
    {
        candidateDetails.SchoolLeavingYear = [newItem.Description intValue];
        [_uxGraduationYearButton setTitle:newItem.Description forState:UIControlStateNormal];
        [_GraduationYearPopOver dismissPopoverAnimated:YES];
        _GraduationYearPopOver = nil;
    }
    
    if(_CourseLevelPopOver)
    {
        candidateDetails.CourseLevelId = newItem.Id;
        [_uxCourseLevelButton setTitle:newItem.Description forState:UIControlStateNormal];
        [_CourseLevelPopOver dismissPopoverAnimated:YES];
        _CourseLevelPopOver = nil;
    }
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (IBAction)CreateRegistration:(id)sender
{
    [self.view endEditing:YES];
    BOOL problemsFound = false;
    NSString *errorString = @"";
    NSString *trimmedString = [NSString stringWithString:firstNameKeyboardView.text];
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedString caseInsensitiveCompare:@""] == NSOrderedSame || [trimmedString caseInsensitiveCompare:CONST_FIRSTNAME] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"A first name must be specified\n"];
        problemsFound = true;
    }
    
    trimmedString = [NSString stringWithString:lastNameKeyboardView.text];
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedString caseInsensitiveCompare:@""] == NSOrderedSame || [trimmedString caseInsensitiveCompare:CONST_LASTNAME] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"A last name must be specified\n"];
        problemsFound = true;
    }      [self.view endEditing:YES];
    
    trimmedString = [NSString stringWithString:emailNameKeyboardView.text];
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedString caseInsensitiveCompare:@""] == NSOrderedSame || [trimmedString caseInsensitiveCompare:CONST_EMAILADDRESS] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"An email address must be specified\n"];
        problemsFound = true;
    }
    if ([self validateEmailWithString:trimmedString]==false)
    {
        errorString = [errorString stringByAppendingString:@"The email address does not appear to be valid. Please check\n"];
        problemsFound = true;
    }
    
    if (problemsFound)
    {
        MessageOverlayVC *messageController = [self.storyboard instantiateViewControllerWithIdentifier: @"MessageOverlay"];
        messageController.ShowCloseButton = true;
        messageController.ContainerView = self.view;
        messageController.Title = @"Oops!";
        messageController.Message = errorString;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];

        
    }
    else
    {
        NSString* error = self.AddUpdateRegistrant;
        
        if (error != nil)
        {
            MessageOverlayVC *messageController = [self.storyboard instantiateViewControllerWithIdentifier: @"MessageOverlay"];
            messageController.ShowCloseButton = TRUE;
            messageController.ContainerView = self.view;
            messageController.Title = @"User details saved!";
            messageController.Message = @"Click the back button to return";
            self.messageOverlay = messageController;
            [self.view addSubview:messageOverlay.view];
        }
        else
        {
            // try saving error to DB
            MessageOverlayVC *messageController = [self.storyboard instantiateViewControllerWithIdentifier: @"MessageOverlay"];
            messageController.ShowCloseButton = TRUE;
            messageController.ContainerView = self.view;
            messageController.Title = @"Oops!";
            messageController.Message = @"An error occured whist trying to save to the DB";
            self.messageOverlay = messageController;
            [self.view addSubview:messageOverlay.view];
        }
        
    }
}

- (NSString*)AddUpdateRegistrant
{
    RegistrationHelper * dbHelper = [[RegistrationHelper alloc] init];
    
    candidateDetails.EventId = _EventId;
    candidateDetails.UploadStatus = 0;
    candidateDetails.FirstName = firstNameKeyboardView.text;
    candidateDetails.LastName = lastNameKeyboardView.text;
    candidateDetails.Email = emailNameKeyboardView.text;
    candidateDetails.DateCreated = [NSDate date];
    
    return self.UpdateOnly?[dbHelper UpdateCandidateDetails:candidateDetails]:[dbHelper CreateNewCandidate:candidateDetails];
}

@end
