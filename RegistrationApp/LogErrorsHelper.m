//
//  LogErrorsHelper.m
//  RegistrationApp
//
//  Created by Rob Taylor on 04/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "LogErrorsHelper.h"
#import "ErrorLogItem.h"

@implementation LogErrorsHelper

+ (void) saveError:(NSString*) errorDescription
{
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    
    NSError *error = [sqlManager doQuery:[NSString stringWithFormat: @"INSERT INTO ErrorLog (Details) values ('%@')", errorDescription]];
    
    if (error!=nil)
    {
        NSString *errorMessage = [NSString stringWithFormat: @"Error: failed to get error list %@.", error.debugDescription];
        NSLog(@"%@",errorMessage);
    }
}

-(NSMutableArray *) loadErrorList
{
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    
    NSArray *rows = [sqlManager getRowsForQuery:@"SELECT ID, DateCreated, Details FROM ErrorLog ORDER BY DateCreated DESC"];
    
    NSMutableArray *errorsArray = [[NSMutableArray alloc] init];
    
    if (rows!=nil)
    {
        for (int count = 0; count<rows.count; count++) {
            
            ErrorLogItem *errorItem = [[ErrorLogItem alloc] init];
            errorItem.rowId = (int) [[rows objectAtIndex:count] objectForKey:@"ID"];
            
            // Convert string to date object
            NSString *dateStr = [[rows objectAtIndex:count] objectForKey:@"DateCreated"];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *date = [dateFormat dateFromString:dateStr];
            [errorItem setErrorDate: date];
            
            
            NSString *errorDetails = [[rows objectAtIndex:count] objectForKey:@"Details"];
            [errorItem setErrorDescription: errorDetails];
            
            [errorsArray addObject:errorItem];
        }
    }
    else
    {
        [LogErrorsHelper saveError: @"Error: failed to get error list"];
    }
    return errorsArray;
}

- (void)clearErrorList
{
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    
    NSError *error = [sqlManager doQuery:@"DELETE FROM ErrorLog"];
    
    if (error!=nil)
    {
        NSString *errorMessage = [NSString stringWithFormat: @"Error: failed to clear error list %@.", error.debugDescription];
        [LogErrorsHelper saveError: errorMessage];
    }
}


@end
