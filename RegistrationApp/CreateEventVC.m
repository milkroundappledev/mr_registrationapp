//
//  CreateEventVC.m
//  RegistrationApp
//
//  Created by Afzal Valiji on 28/01/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "CreateEventVC.h"



@interface CreateEventVC ()

@end

@implementation CreateEventVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    [_uxSelectDateButton addTarget:self action:@selector(dateButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_uxSelectDateButton];
    if(_EventId >0)
    {
        _IsEditMode = true;
        CandidateEventsHelper *eventHelper = [[CandidateEventsHelper alloc]init];
        CandidateEvent *event = [eventHelper GetCandiateEventById:_EventId];
        _uxEventName.text = event.EventName;
        _uxEventDate.text = [FormatHelper GetStringFromDate:event.EventDate];
    }
    else
        _uxEventDate.text = [FormatHelper GetStringFromDate:[NSDate date]];

}
- (void)dateButtonAction:(UIButton *)sender
{
    UIViewController *viewController = [[UIViewController alloc]init];
    UIView *viewForDatePicker = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 200, 100)];
    
    datepicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, 200, 100)];
    datepicker.datePickerMode = UIDatePickerModeDate;
    datepicker.hidden = NO;
    datepicker.date = [NSDate date];
    [datepicker addTarget:self action:@selector(LabelChange:) forControlEvents:UIControlEventValueChanged];
    
    [viewForDatePicker addSubview:datepicker];
    [viewController.view addSubview:viewForDatePicker];
    
    popOverForDatePicker = [[UIPopoverController alloc]initWithContentViewController:viewController];
    popOverForDatePicker.delegate = self;
    [popOverForDatePicker setPopoverContentSize:CGSizeMake(200, 100) animated:NO];
    [popOverForDatePicker presentPopoverFromRect:sender.frame inView:self.view  permittedArrowDirections:(UIPopoverArrowDirectionUp) animated:YES];
}

-(void)LabelChange:(id)sender
{
    self.uxEventDate.text =[FormatHelper GetStringFromDate:datepicker.date];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction) CancelButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}
- (IBAction) SaveButtonClicked:(id)sender{

    
    if(_uxEventName.text ==nil || [_uxEventName.text length]<=0)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message: @"Please enter the name" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        
        CandidateEventsHelper *helper = [[CandidateEventsHelper alloc]init];
        bool saved ;
        if(_IsEditMode)
        {
            CandidateEvent *event = [helper GetCandiateEventById:_EventId];
            event.EventName = _uxEventName.text;
            event.EventDate = [FormatHelper GetDateFromString:_uxEventDate.text];
            saved = [helper EditCandidateEvent:event];
        }
        else
        {
            CandidateEvent *event =[[CandidateEvent alloc]init];
            event.EventName = _uxEventName.text;
            event.EventDate = [FormatHelper GetDateFromString:_uxEventDate.text];

            saved = [helper CreateCandidateEvent:event];
        }
            if(saved)
        {
          
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Event" message: @"Event saved successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [_saveDelegate RefreshTableView];
            [self dismissViewControllerAnimated:true completion:nil];
        }
    }
}

@end
