//
//  RegistrationHelper.h
//  RegistrationApp
//
//  Created by Afzal Valiji on 01/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQLiteManager.h"
#import "CandidateDetails.h"
#import "PopOverListItem.h"
#import "LogErrorsHelper.h"
#import "WebserviceUtils.h"
#import "FormatHelper.h"

@interface RegistrationHelper : NSObject

-(NSMutableArray *) getAllCourseLevelList;
-(NSString *)CreateNewCandidate:(CandidateDetails*)candidateDetails;
- (NSString*)UpdateCandidateDetails:(CandidateDetails*)details;
-(NSMutableArray *) GetAllCandidateByEventId:(int) EventId;
-(BOOL) uploadCandidates:(int)eventId;

@end
