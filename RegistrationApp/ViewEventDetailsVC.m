//
//  ViewEventDetailsVC.m
//  RegistrationApp
//
//  Created by Afzal Valiji on 07/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "ViewEventDetailsVC.h"

@interface ViewEventDetailsVC ()

@end


@implementation ViewEventDetailsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    RegistrationHelper *candidateHelper = [[RegistrationHelper alloc]init];
    _CandidateList = [candidateHelper GetAllCandidateByEventId:_EventId];

    CandidateEventsHelper *eventHelper = [[CandidateEventsHelper alloc]init];
    CandidateEvent *event = [eventHelper  GetCandiateEventById:_EventId];
    _uxEventName.text = event.EventName;
    _uxEventDate.text = [FormatHelper GetStringFromDate:event.EventDate];
    _uxTotalCount.text = [NSString stringWithFormat:@"%d", event.TotalRegCount];
    _uxUploadedCount.text = [NSString stringWithFormat:@"%d", event.UploadedRegCount];
    _uxFailedCount.text = [NSString stringWithFormat:@"%d", event.FailedRegCount];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_CandidateList count];
}

- (CandidateDetailVCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CandidateDetailVCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[CandidateDetailVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
   
    CandidateDetails * candidate = [_CandidateList objectAtIndex:indexPath.row];
    
    cell.rowId = candidate.rowId;
    cell.uxFirstName.text = candidate.FirstName;
    cell.uxLastName.text = candidate.LastName;
    cell.uxEmail.text = candidate.Email;
    cell.uxCourseLevel.text = candidate.CourseName;
    cell.uxSchoolLeavingYear.text =[NSString stringWithFormat:@"%d",candidate.SchoolLeavingYear];
    if(candidate.UploadStatus == 0)
    {
        [cell.uxUploadStatus setImage:[UIImage imageNamed: @"mini-cross.png"] forState:UIControlStateNormal];
        cell.uxEditCandidateButton.hidden = NO;
    }
    else
    {
        [cell.uxUploadStatus setImage:[UIImage imageNamed: @"mini-tick.png"] forState:UIControlStateNormal];
        cell.uxEditCandidateButton.hidden = YES;
    }
    cell.delegate = self;

    return cell;
}


-(void) EditCandidateButtonClicked:(NSInteger)rowId
{
    RegistrationVC *destController = [self.storyboard instantiateViewControllerWithIdentifier: @"Registration"];
    CandidateDetails * candidate = [_CandidateList objectAtIndex:rowId];
    destController.candidateDetails = candidate;
    [self presentViewController:destController animated:true completion:nil];
}

-(BOOL) GetSortOrder{
    if(_AscendingOrder == NO)
        self.AscendingOrder = YES;
    else
        self.AscendingOrder = NO;
    return self.AscendingOrder;
}
- (IBAction)FirstNameHeaderClicked:(id)sender {
    _SortBy = @"_FirstName";
    [self SortByField:_SortBy];
}
- (IBAction)LastNameHeaderClicked:(id)sender {
    _SortBy = @"_LastName";
    [self SortByField:_SortBy];
}
- (IBAction)EmailHeaderClicked:(id)sender {
    _SortBy = @"_Email";
    [self SortByField:_SortBy];
}
- (IBAction)CourseLevelHeaderClicked:(id)sender {
    _SortBy = @"_CourseName";
    [self SortByField:_SortBy];
}
- (IBAction)SchoolLeavingYearHeaderClicked:(id)sender {
    _SortBy = @"_SchoolLeavingYear";
    [self SortByField:_SortBy];
}
- (IBAction)UploadStatusHeaderClicked:(id)sender {
    _SortBy = @"_UploadStatus";
    [self SortByField:_SortBy];
}

-(void) SortByField:(NSString *) sortBy
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortBy
                                                                   ascending:[self GetSortOrder]];
    NSMutableArray *sortDescriptors = [NSMutableArray arrayWithObject:sortDescriptor];
    [_CandidateList sortUsingDescriptors:sortDescriptors];
    [self.candidateTableVIew reloadData];
}

- (IBAction) BackButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:true completion:nil];
}
@end
