//
//  ErrorLogListViewController.m
//  RegistrationApp
//
//  Created by Rob Taylor on 05/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "ErrorLogListVC.h"

@interface ErrorLogListVC ()

@end

@implementation ErrorLogListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
