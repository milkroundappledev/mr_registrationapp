//
//  CandidateEventsHelper.m
//  RegistrationApp
//
//  Created by Afzal Valiji on 30/01/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "CandidateEventsHelper.h"
#import "SQLiteManager.h"

@implementation CandidateEventsHelper

-(bool) CreateCandidateEvent: (CandidateEvent *)event{

    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    
    [sqlManager openDatabase];
    
    NSString *query = [NSString stringWithFormat: @"INSERT INTO Events (EventName, EventDate) values ('%@','%@')", event.EventName, event.EventDate];
     [sqlManager doQueryWithoutTran:query];
    return true;
}

-(bool) EditCandidateEvent: (CandidateEvent *)event{
    
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    
    [sqlManager openDatabase];
    
    NSString *query = [NSString stringWithFormat: @"UPDATE Events SET EventName = '%@',EventDate = '%@' WHERE id = %d", event.EventName, event.EventDate,event.Id];
    [sqlManager doQueryWithoutTran:query];
    return true;
}

-(NSArray *) GetAllCandidateEvents{
    
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    NSString *query = [[NSString alloc]init];
    NSArray *eventArray = [sqlManager getRowsForQuery:@"select * from Events"];
    
    NSMutableArray * arrayOfEvents = [[NSMutableArray alloc] init];
    
    for (NSMutableDictionary * dbEvent in eventArray) {
        CandidateEvent * event = [[CandidateEvent alloc] init];
        
        event.Id = [[dbEvent objectForKey:@"Id" ]  intValue];
        event.EventName = [dbEvent objectForKey:@"EventName"];
        
        NSString *dateString = [[NSString alloc] initWithUTF8String:[[dbEvent objectForKey:@"EventDate"] UTF8String]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss ZZZZ"];
        event.EventDate = [formatter dateFromString:dateString];
        
        query = [NSString stringWithFormat: @"SELECT COUNT(*) FROM Registrations Where EventId= %d", event.Id];
        event.TotalRegCount = [sqlManager GetDataCount:query];
        
        query = [NSString stringWithFormat: @"SELECT COUNT(*) FROM Registrations Where EventId= %d AND UploadStatus = 1", event.Id];
        event.UploadedRegCount = [sqlManager GetDataCount:query];

        query = [NSString stringWithFormat: @"SELECT COUNT(*) FROM Registrations Where EventId= %d AND UploadStatus = 0", event.Id];
        event.FailedRegCount = [sqlManager GetDataCount:query];
        
        [arrayOfEvents addObject:event];
    }
    
    return arrayOfEvents;
}

-(CandidateEvent *) GetCandiateEventById:(int) identifier{
    
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    
    NSString *query = [NSString stringWithFormat: @"SELECT * FROM Events WHERE id = %d", identifier];
    NSArray *eventArray = [sqlManager getRowsForQuery:query];
    NSMutableDictionary * dbEvent= [eventArray objectAtIndex: 0];
    
    CandidateEvent * event = [[CandidateEvent alloc] init];
    event.Id = [[dbEvent objectForKey:@"Id" ]  intValue];
    event.EventName = [dbEvent objectForKey:@"EventName"];

    NSString *dateString = [[NSString alloc] initWithUTF8String:[[dbEvent objectForKey:@"EventDate"] UTF8String]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss ZZZZ"];
    event.EventDate = [formatter dateFromString:dateString];
    
    
    query = [NSString stringWithFormat: @"SELECT COUNT(*) FROM Registrations Where EventId= %d", identifier];
    event.TotalRegCount = [sqlManager GetDataCount:query];
    
    query = [NSString stringWithFormat: @"SELECT COUNT(*) FROM Registrations Where EventId= %d AND UploadStatus = 1", identifier];
    event.UploadedRegCount = [sqlManager GetDataCount:query];
    
    query = [NSString stringWithFormat: @"SELECT COUNT(*) FROM Registrations Where EventId= %d AND UploadStatus = 0", identifier];
    event.FailedRegCount = [sqlManager GetDataCount:query];
    
    return event;
}
@end
