//
//  ViewEventDetailsVC.h
//  RegistrationApp
//
//  Created by Afzal Valiji on 07/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CandidateDetailVCell.h"
#import "CandidateDetails.h"
#import "RegistrationVC.h"
#import "RegistrationHelper.h"
#import "CandidateEventsHelper.h"
#import "FormatHelper.h"

@interface ViewEventDetailsVC : UIViewController <CandidateDetailCellDelegate>

@property int EventId;
@property (weak, nonatomic) IBOutlet UILabel *uxEventName;
@property (weak, nonatomic) IBOutlet UILabel *uxEventDate;
@property (weak, nonatomic) IBOutlet UILabel *uxTotalCount;
@property (weak, nonatomic) IBOutlet UILabel *uxUploadedCount;
@property (weak, nonatomic) IBOutlet UILabel *uxFailedCount;
@property (strong, nonatomic) NSMutableArray *CandidateList;
@property (weak, nonatomic) IBOutlet UITableView *candidateTableVIew;
@property (strong, nonatomic) NSString *SortBy;
@property (nonatomic, assign) BOOL AscendingOrder;

- (IBAction)SchoolLeavingYearHeaderClicked:(id)sender;
- (IBAction)UploadStatusHeaderClicked:(id)sender;
- (IBAction)CourseLevelHeaderClicked:(id)sender;
- (IBAction)EmailHeaderClicked:(id)sender;
- (IBAction)LastNameHeaderClicked:(id)sender;
- (IBAction)FirstNameHeaderClicked:(id)sender;
- (IBAction)BackButtonClicked:(id)sender;
@end
