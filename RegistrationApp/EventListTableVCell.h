//
//  EventListTableVCell.h
//  RegistrationApp
//
//  Created by Afzal Valiji on 05/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateEventVC.h"

@protocol EventListTableCellDelegate <NSObject>

-(void)EditEventButtonClicked:(NSInteger)rowId;
-(void)ViewEventDetailsButtonClicked:(NSInteger)rowId;
-(void)RegisterButtonClicked:(NSInteger)rowId;
-(void)UploadButtonClicked:(NSInteger)rowId;

@end

@interface EventListTableVCell : UITableViewCell

@property (nonatomic,assign) int rowId;
@property (weak, nonatomic) IBOutlet UILabel *uxEventName;
@property (weak, nonatomic) IBOutlet UILabel *uxEventDate;
@property (weak, nonatomic) IBOutlet UIButton *uxEditButton;
@property (weak, nonatomic) IBOutlet UILabel *uxTotalCount;
@property (weak, nonatomic) IBOutlet UILabel *uxUploadedCount;
@property (weak, nonatomic) IBOutlet UILabel *uxFailedCount;

@property(nonatomic,assign)id <EventListTableCellDelegate> delegate;
- (IBAction)EditButtonPressed:(id)sender;
- (IBAction)ViewDetailsButtonPressed:(id)sender;
- (IBAction)RegisterButtonPressed:(id)sender;

@end


