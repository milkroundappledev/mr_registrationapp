//
//  CandidateDetailVCell.h
//  RegistrationApp
//
//  Created by Afzal Valiji on 08/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CandidateDetailCellDelegate <NSObject>

-(void) EditCandidateButtonClicked:(NSInteger) rowId;

@end

@interface CandidateDetailVCell : UITableViewCell

@property int rowId;
@property (weak, nonatomic) IBOutlet UILabel *uxFirstName;
@property (weak, nonatomic) IBOutlet UILabel *uxLastName;
@property (weak, nonatomic) IBOutlet UILabel *uxEmail;
@property (weak, nonatomic) IBOutlet UILabel *uxCourseLevel;
@property (weak, nonatomic) IBOutlet UILabel *uxSchoolLeavingYear;
@property (weak, nonatomic) IBOutlet UIButton *uxUploadStatus;
@property (weak, nonatomic) IBOutlet UIButton *uxEditCandidateButton;

@property(nonatomic,assign)id <CandidateDetailCellDelegate> delegate;
- (IBAction)EditButtonPressed:(id)sender;

@end
