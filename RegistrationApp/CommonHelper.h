//
//  CommonHelper.h
//  RegistrationApp
//
//  Created by Mounir on 30/01/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonHelper : NSObject
{
    
}

+ (CommonHelper *)GlobalHelper;

-(void)DisplayMessageBox: (NSString*)Message withMessageTitle:(NSString *)Title andCancelBtnMsg:(NSString*)CancelText;
-(NSString*)DateToString: (NSDate*)Date;
//int a = [MySingleton sharedSingleton].prop
//
//[[MySingleton sharedSingleton] method];

@end
