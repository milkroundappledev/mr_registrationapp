//
//  CandidateEvent.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CandidateEvent : NSObject

@property int Id;
@property (strong, nonatomic) NSString *EventName;
@property (strong, nonatomic) NSDate *EventDate;
@property int TotalRegCount;
@property int UploadedRegCount;
@property int FailedRegCount;
@end
