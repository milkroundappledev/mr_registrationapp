//
//  MessageOverlayVC.m
//  RegistrationApp
//
//  Created by Vidhya Alagar on 15/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "MessageOverlayVC.h"
#import "QuartzCore/QuartzCore.h"


@interface MessageOverlayVC ()

@end


@implementation MessageOverlayVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIColor *bg = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    self.view.backgroundColor = bg;
    
    if (_Title != nil && _Title.length > 0)
        [_uxTitleLabel setText:_Title];
    
    if (_Message != nil && _Message.length > 0)
        [_uxContentTextView setText:_Message];
    
    if (_ShowCloseButton)
    {
        _uxCloseButton.hidden = false;
        //overlayView.frame = CGRectMake(192, 387, 385, 230);
    }
    else
    {
        _uxCloseButton.hidden = true;
        //overlayView.frame = CGRectMake(192, 387, 385, 200);
    }
    
    if (_AutoCloseTimeout>0)
    {
        //set the timer
        self.timer = [NSTimer scheduledTimerWithTimeInterval:_AutoCloseTimeout
                                                      target:self
                                                    selector:@selector(timerCalled)
                                                    userInfo:nil
                                                     repeats:NO];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

-(IBAction)CloseButtonClicked:(id)sender
{
    [self CloseOverlay];
}

-(void)CloseOverlay
{
    // get the current view
    UIView *currentView = self.view;
    
    // remove the current view and replace with myView1
	[currentView removeFromSuperview];
    
    // set up an animation for the transition between the views
	CATransition *animation = [CATransition animation];
	[animation setDuration:0.5];
	[animation setType:kCATransitionFade];
	[animation setSubtype:kCATransitionFromLeft];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];

	[[_ContainerView layer] addAnimation:animation forKey:@"CloseMessageOverlay"];
    
}

-(void)TimerCalled
{
    [self.Timer invalidate];
    self.Timer = nil; // ensures we never invalidate an already invalid Timer
    
    [self CloseOverlay];
}
@end
