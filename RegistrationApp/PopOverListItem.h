//
//  PopOverListItem.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 26/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PopOverListItem : NSObject

@property(nonatomic,copy) NSString *Description;
@property(readwrite,assign) int Id;

@end
