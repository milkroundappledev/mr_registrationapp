//
//  Constants.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 06/09/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "Constants.h"

@implementation Constants
NSString * const CONST_FIRSTNAME = @"First name";
NSString * const CONST_LASTNAME = @"Last name";
NSString * const CONST_EMAILADDRESS = @"your.email@any.domain.com";

@end
