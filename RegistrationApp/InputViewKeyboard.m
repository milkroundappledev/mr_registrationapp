//
//  InputViewKeyboard.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 28/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "InputViewKeyboard.h"

@implementation InputViewKeyboard


/*************************************************
 
 * fixes the issue with single lined uitextview
 
 *************************************************/

- (UIEdgeInsets) contentInset { return UIEdgeInsetsZero; }

- (UIView *)inputView {
    if (!inputView) {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"InputViewKeyboard" owner:self
                                                       options:nil];
        inputView = [objects objectAtIndex:0];
    }
    return inputView;
}

- (IBAction)takeInputFromTitle:(id)sender {
    NSRange r = self.selectedRange;
    int cursorPos = r.location;
    if (self.text.length == 0)
        self.text = [self.text stringByReplacingCharactersInRange:self.selectedRange
                                                       withString:[((UIButton *)sender).currentTitle uppercaseString]];
    else if (cursorPos>0 && [self.text characterAtIndex:cursorPos-1] == ' ')
        self.text = [self.text stringByReplacingCharactersInRange:self.selectedRange
                                                       withString:[((UIButton *)sender).currentTitle uppercaseString]];
    else 
        self.text = [self.text stringByReplacingCharactersInRange:self.selectedRange
                                                       withString:[((UIButton *)sender).currentTitle lowercaseString]]; 
    
    r.location+=[((UIButton *)sender).currentTitle length];
    r.length = 0;
    self.selectedRange = r;
}



- (IBAction)doDelete:(id)sender {
    NSRange r = self.selectedRange;
    if (r.length > 0) {
        // the user has highlighted some text, fall through to delete it
    } else {
        // there's just an insertion point
        if (r.location == 0) {
            // cursor is at the beginning, forget about it.
            return;
        } else {
            r.location -= 1;
            r.length = 1;
        }
    }
    self.text = [self.text stringByReplacingCharactersInRange:r withString:@""];
    r.length = 0;
    self.selectedRange = r;
}

@end
