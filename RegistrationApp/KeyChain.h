//
//  KeyChain.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 24/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyChain : NSObject {
}

+ (BOOL)setString:(NSString *)string forKey:(NSString *)key;
+ (NSString *)getStringForKey:(NSString *)key;

@end