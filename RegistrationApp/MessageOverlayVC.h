//
//  MessageOverlayVC.h
//  RegistrationApp
//
//  Created by Vidhya Alagar on 15/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MessageOverlayVC : UIViewController

@property (nonatomic, strong) IBOutlet UIButton *uxCloseButton;
@property (nonatomic, strong) IBOutlet UILabel *uxTitleLabel;
@property (nonatomic, strong) IBOutlet UITextView *uxContentTextView;
@property (nonatomic, strong) IBOutlet UIImageView *uxImageView;

@property (nonatomic, copy) NSString *Title;
@property (nonatomic, copy) NSString *Message;
@property (nonatomic, assign) BOOL ShowCloseButton;
@property (nonatomic, assign) int AutoCloseTimeout;
@property (nonatomic, strong) NSTimer *Timer;

@property (nonatomic, strong) UIView *ContainerView;
@property (nonatomic, strong) IBOutlet UIView *OverlayView;

-(IBAction)CloseButtonClicked:(id)sender;
-(void)TimerCalled;
-(void)CloseOverlay;

@end
