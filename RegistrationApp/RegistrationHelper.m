//
//  RegistrationHelper.m
//  RegistrationApp
//
//  Created by Afzal Valiji on 01/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "RegistrationHelper.h"
#import "CandidateDetails.h"
#import "FormatHelper.h"

@implementation RegistrationHelper

-(NSMutableArray *) getAllCourseLevelList{
    
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    
    NSArray *courseLevelArray = [sqlManager getRowsForQuery:@"select * from CourseLevel"];
    
    NSMutableArray * arrayOfCourseLevel = [[NSMutableArray alloc] init];
    
    for (NSMutableDictionary * dbCourse in courseLevelArray) {
        PopOverListItem * courseItem = [[PopOverListItem alloc] init];
        
        courseItem.Id = [[dbCourse objectForKey:@"Id" ]  intValue];
        courseItem.Description = [dbCourse objectForKey:@"Description"];
        
        [arrayOfCourseLevel addObject:courseItem];
    }
    
    return arrayOfCourseLevel;
}

-(NSMutableArray *) GetAllCandidateByEventId:(int) EventId{
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    NSString *query = [NSString stringWithFormat: @"SELECT C.Description,R.Id,R.EventId,R.FirstName,R.LastName,R.Email,R.SchoolLeavingYear,R.UploadStatus,R.CourseLevelId FROM Registrations R inner join CourseLevel C on C.id=R.CourseLevelid Where EventId= %d", EventId];
    
    NSArray *candidateArray = [sqlManager getRowsForQuery:query];
    
    NSMutableArray * arrayOfCandidates = [[NSMutableArray alloc] init];
    
    for (NSMutableDictionary * dbEvent in candidateArray) {
        CandidateDetails * candidate = [[CandidateDetails alloc] init];
        
        candidate.rowId = [[dbEvent objectForKey:@"Id" ]  intValue];
        candidate.EventId = [[dbEvent objectForKey:@"EventId"] intValue];
        candidate.FirstName = [dbEvent objectForKey:@"FirstName"];
        candidate.LastName = [dbEvent objectForKey:@"LastName"];
        candidate.Email = [dbEvent objectForKey:@"Email"];
        candidate.SchoolLeavingYear = [[dbEvent objectForKey:@"SchoolLeavingYear"] intValue];
        candidate.UploadStatus = [[dbEvent objectForKey:@"UploadStatus"] intValue];
        candidate.CourseName = [dbEvent objectForKey:@"Description"];
        candidate.CourseLevelId = [[dbEvent objectForKey:@"CourseLevelId"] intValue];
        [arrayOfCandidates addObject:candidate];
    }
    
    return arrayOfCandidates;
}
-(NSString *)CreateNewCandidate:(CandidateDetails*)candidateDetails
{
    NSString *query = [NSString stringWithFormat: @"INSERT INTO Registrations (EventId,FirstName,LastName,Email,DateCreated,UploadStatus,SchoolLeavingYear,UploadDate,CourseLevelId) VALUES ('%d','%@','%@','%@','%@',%d,%d,NULL,%d)", candidateDetails.EventId, candidateDetails.FirstName, candidateDetails.LastName, candidateDetails.Email,[FormatHelper GetDBStringFromDate:candidateDetails.DateCreated], candidateDetails.UploadStatus, candidateDetails.SchoolLeavingYear, candidateDetails.CourseLevelId];
    
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    
    return [sqlManager doQueryWithoutTran:query].description;
    
}

- (BOOL) uploadCandidates:(int)eventId
{
    WebserviceUtils *webservice = [[WebserviceUtils alloc] init];
    NSMutableArray *candidateList = [self GetAllCandidateByEventId:eventId];
    BOOL success = false;
    
    if (candidateList!=nil && candidateList.count>0)
    {
        success = true;
        int loop;
        for (loop=0; loop<candidateList.count; loop++)
        {
            CandidateDetails *details = [candidateList objectAtIndex:loop];
            if (details!=nil && details.UploadStatus==0)
            {
                int response = [webservice registerCandidate:@"http://stg.milkround.com":details:@"7":@"a27996fb-2dc3-4e5b-85e5-aba31d6721d6"];
                if (response==1)
                {
                    // update the candidate item as being uploaded successfully
                    details.uploadDate = [NSDate date];
                    details.uploadStatus = 1;
                    [self UpdateCandidateDetails:details];
                }
                
                else
                {
                    // The server timed out
                    NSString *error = nil;
                    
                    if (response == 3) {
                        error = [NSString stringWithFormat:@"Error saving candidate to server: Email: %@, First name: %@, Last name: %@, Response: Server timed out",details.Email,details.FirstName,details.LastName];
                        [LogErrorsHelper saveError:error];
                        return NO;
                    }
                    
                    // the cadidate already exists
                    if (response == 2)
                    {
                        details.uploadStatus = 2;
                        [self UpdateCandidateDetails:details];
                        error = [NSString stringWithFormat:@"Error saving candidate to server: Email: %@, First name: %@, Last name: %@, Response: Candidate already exists",details.Email,details.FirstName,details.LastName];
                    }
                    
                    // other errors?
                    else {
                        {
                            error = [NSString stringWithFormat:@"Error saving candidate to server: Email: %@, First name: %@, Last name: %@, Possible Error with E-Mail or Details ",details.Email,details.FirstName,details.LastName];
                        }
                        [LogErrorsHelper saveError:error];
                    }
                }
            }
        }
    }
    
    return success;
    
}

- (NSString*)UpdateCandidateDetails:(CandidateDetails*)details
{
    NSString *query = [NSString stringWithFormat: @"UPDATE Registrations SET EventId=%d,CourseLevelId=%d, SchoolLeavingYear=%d, FirstName='%@', LastName='%@', Email='%@', UploadStatus=%d, UploadDate='%@' WHERE ID=%d", details.EventId, details.CourseLevelId, details.SchoolLeavingYear, details.FirstName, details.LastName, details.Email, details.UploadStatus, details.UploadDate, details.rowId];
    
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    
    NSError *error = [sqlManager doQuery:query];
    
    return error.description;
}

@end
