//
//  RegistrationVC.h
//  RegistrationApp
//
//  Created by Afzal Valiji on 01/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegistrationHelper.h"
#import "PopOverGenericVC.h"
#import "InputViewKeyboard.h"
//#import "FeedbackMessageOverlay.h"
#import "MessageOverlayVC.h"


@interface RegistrationVC : UIViewController<ListViewPopOverControllerDelegate, UITextViewDelegate>

@property (strong,nonatomic) NSMutableArray *CourseLevelList;
@property (strong,nonatomic) NSMutableArray *GraduationYearList;
@property (nonatomic, strong) PopOverGenericVC * GraduationYearPopOverVC;
@property (nonatomic, strong) PopOverGenericVC * CourseLevelPopOverVC;
@property (nonatomic, strong) UIPopoverController * GraduationYearPopOver;
@property (nonatomic, strong) UIPopoverController * CourseLevelPopOver;
@property (strong, nonatomic) IBOutlet UIButton *uxCourseLevelButton;
@property (strong, nonatomic) IBOutlet UIButton *uxGraduationYearButton;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *firstNameKeyboardView;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *emailNameKeyboardView;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *lastNameKeyboardView;
@property (nonatomic, strong) CandidateDetails * candidateDetails;
@property (nonatomic, strong) MessageOverlayVC *messageOverlay;
@property int EventId;
@property bool UpdateOnly;

- (IBAction)CourseLevelClicked:(id)sender;
- (IBAction)GraduationYearClicked:(id)sender;
- (IBAction)CancelButtonClicked:(id)sender;
- (IBAction)CreateRegistration:(id)sender;

- (BOOL)validateEmailWithString:(NSString*)email;
- (void)GenerateGraduationYearList;
-(void)GetCourseLevelListFromDB;

@end

