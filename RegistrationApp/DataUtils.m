//
//  DataUtils.m
//  Candidate Registrations
//
//

#import "DataUtils.h"
#import "CandidateEvent.h"
#import "CandidateDetails.h"
#import "PopOverListItem.h"
#import "ErrorLogItem.h"
#import "LogErrorsHelper.h"



@implementation DataUtils

- (int) getCandidateCounts:(BOOL)isUploaded :(int)eventId
{
    NSString *query = nil;
    int count = 0;
    
    if (isUploaded == 1)
        query = [NSString stringWithFormat: @"SELECT COUNT(ID) AS 'COUNT' FROM Registrations WHERE EventId=%d AND UploadStatus=1",eventId];
    else if (isUploaded == 0)
        query = [NSString stringWithFormat: @"SELECT COUNT(ID) AS 'COUNT' FROM Registrations WHERE EventId=%d AND UploadStatus=0",eventId];
    else if (isUploaded == 2)
        query = [NSString stringWithFormat: @"SELECT COUNT(ID) AS 'COUNT' FROM Registrations WHERE EventId=%d AND UploadStatus=2",eventId];
    
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    
    NSArray *rows = [sqlManager getRowsForQuery:query];

    if (rows!=nil && rows.count>0)
    {
            count = (int) [[rows objectAtIndex:0] objectForKey:@"COUNT"];
    }
    else
    {
            [LogErrorsHelper saveError: @"Error: failed to get upload counts."];
    }
    
    return count;
}


- (NSMutableArray *)loadCandidateList:(int)eventId
{
    NSString *query = [NSString stringWithFormat: @"SELECT r.Id, r.EventId, r.UniversityId, r.CourseId, r.FirstName, r.LastName, r.Email, r.DateCreated, r.UploadStatus, r.GraduationYear, u.Description AS UniName, c.Description AS CourseName FROM Registrations r left outer join Universities u on u.id=r.UniversityId left outer join Courses c on c.id=r.CourseId WHERE r.EventId = %d",eventId];
    
    SQLiteManager * sqlManager = [[SQLiteManager alloc] initWithDefaultDatabase];
    [sqlManager openDatabase];
    
    NSArray *rows = [sqlManager getRowsForQuery:query];
    NSMutableArray *candidatesArray = [[NSMutableArray alloc] init];

    
    if (rows!=nil)
    {
        for (int count = 0; count<rows.count; count++) {
    
            CandidateDetails *item = [[CandidateDetails alloc] init];
            item.rowId = (int) [[rows objectAtIndex:count] objectForKey:@"ID"];
            item.eventId = (int) [[rows objectAtIndex:count] objectForKey:@"EventId"];
//            item.universityId = (int) [[rows objectAtIndex:count] objectForKey:@"UniversityId"];
            item.courseLevelId = (int) [[rows objectAtIndex:count] objectForKey:@"CourseLevelId"];
            
            NSString *firstName = [[rows objectAtIndex:count] objectForKey:@"FirstName"];
            [item setFirstName:firstName];

            NSString *lastName = [[rows objectAtIndex:count] objectForKey:@"LastName"];
            [item setLastName:lastName];

            NSString *email = [[rows objectAtIndex:count] objectForKey:@"Email"];
            [item setEmail:email];

            NSString *dateCreatedStr = [[rows objectAtIndex:count] objectForKey:@"DateCreated"];
            
            // Convert string to date object
            NSDateFormatter *dateCreatedFormat = [[NSDateFormatter alloc] init];
            [dateCreatedFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *dateCreated = [dateCreatedFormat dateFromString:dateCreatedStr];  
            
            item.dateCreated = dateCreated;
            
            item.UploadStatus = (int)[[rows objectAtIndex:count] objectForKey:@"UploadStatus"];
            item.SchoolLeavingYear = (int)[[rows objectAtIndex:count] objectForKey:@"SchoolLeavingYear"];

//            if ([[rows objectAtIndex:count] objectForKey:@"UniversityName"]!=nil)
//            {
//                NSString *universityName = [[rows objectAtIndex:count] objectForKey:@"UniversityName"];
//                [item setUniversityName:universityName];
//            }
//            else
//                [item setUniversityName:@"University not specified"];
//
            if ([[rows objectAtIndex:count] objectForKey:@"CourseName"]!=nil)
            {
                NSString *courseName = [[rows objectAtIndex:count] objectForKey:@"CourseName"];
                [item setCourseName:courseName];
            }
            else
                [item setCourseName:@"Course not specified"];

            NSString *dateUploadedStr = [[rows objectAtIndex:count] objectForKey:@"DateCreated"];
            
            // Convert string to date object
            NSDateFormatter *dateUploadedFormat = [[NSDateFormatter alloc] init];
            [dateUploadedFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *dateUploaded = [dateUploadedFormat dateFromString:dateUploadedStr];  
            
            item.uploadDate = dateUploaded;
            [candidatesArray addObject:item];
            
        }
        
    }
    else
    {
        [LogErrorsHelper saveError: @"Error: failed to get candidate list"];
    }
    
    return candidatesArray;
}






- (NSString*)getProviderId
{
    NSString *serverId = [KeyChain getStringForKey:@"milkround-provider-id"];
    if (serverId == nil)
    {
        [self setProviderId:@"1":@"DD208A11-2A89-4b6a-A42D-C83255775D40"];
        serverId = [KeyChain getStringForKey:@"milkround-provider-id"];
    }
    return serverId;
}


- (NSString*)getProviderPassword
{
    NSString *serverId = [KeyChain getStringForKey:@"milkround-provider-password"];
    if (serverId == nil)
    {
        serverId = @"";
    }
    return serverId;
}

- (BOOL)setProviderId:(NSString*)providerId :(NSString*)providerPassword;
{
    [KeyChain setString:providerId forKey:@"milkround-provider-id"];
    [KeyChain setString:providerPassword forKey:@"milkround-provider-password"];
    return true;
}

- (BOOL)checkPassword:(NSString*)password
{
    NSString* standardPassword = [KeyChain getStringForKey:@"milkround-standard-user"];
    NSString* adminPassword = [KeyChain getStringForKey:@"milkround-it-user"];
    
    // first time software is run, so need to create initial passwords
    if (standardPassword==nil)
    {
        [self resetPassword:FALSE];
        standardPassword = [KeyChain getStringForKey:@"milkround-standard-user"];
    }
    
    if (adminPassword==nil)
    {
        [self resetPassword:TRUE];
        adminPassword = [KeyChain getStringForKey:@"milkround-it-user"];
    }
    
    if ([standardPassword caseInsensitiveCompare:password] == NSOrderedSame)
    {
        return TRUE;
    }
    else if ([adminPassword caseInsensitiveCompare:password] == NSOrderedSame)
    {
        return TRUE;
    }
    
    return FALSE;
}

-(void)resetPassword:(BOOL)isITAdmin
{
    if (isITAdmin)
        [KeyChain setString:@"02030034059" forKey:@"milkround-it-user"];
    else
        [KeyChain setString:@"milkround" forKey:@"milkround-standard-user"];
}

-(BOOL)saveNewPassword:(NSString*)oldPassword :(NSString*)newPassword
{
    // first check that the password is for standard user
    NSString* standardPassword = [KeyChain getStringForKey:@"milkround-standard-user"];
    if ([standardPassword caseInsensitiveCompare:oldPassword] != NSOrderedSame)
    {
        // now check if that failed. Was user entering the IT ADMIN password (for resetting the standard user password?
        NSString* adminPassword = [KeyChain getStringForKey:@"milkround-it-user"];
        if ([adminPassword caseInsensitiveCompare:oldPassword] != NSOrderedSame)
            return FALSE;
    }
    [KeyChain setString:newPassword forKey:@"milkround-standard-user"];
    return TRUE;    
}

- (NSString*)getServerName
{
    return [KeyChain getStringForKey:@"server-name"];
}

- (BOOL)setServerName:(NSString*)serverName
{
    [KeyChain setString:serverName forKey:@"server-name"];
    return true;
}

@end
