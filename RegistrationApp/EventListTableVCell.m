//
//  EventListTableVCell.m
//  RegistrationApp
//
//  Created by Afzal Valiji on 05/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "EventListTableVCell.h"

@implementation EventListTableVCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(IBAction)EditButtonPressed :(id)sender {
        //send the delegate function with the amount entered by the user
        [_delegate EditEventButtonClicked: _rowId];
}

-(IBAction)ViewDetailsButtonPressed :(id)sender {
    //send the delegate function with the amount entered by the user
    [_delegate ViewEventDetailsButtonClicked: _rowId];
}
-(IBAction)RegisterButtonPressed:(id)sender
{
    [_delegate RegisterButtonClicked:_rowId];
}
-(IBAction)UploadButtonPressed:(id)sender
{
    [_delegate UploadButtonClicked:_rowId];
}

@end
