//
//  GraduationYearVC.h
//  RegistrationApp
//
//  Created by Mounir on 05/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverListItem.h"

@protocol ListViewPopOverControllerDelegate <NSObject>

-(void)selectedItem:(PopOverListItem*)newItem;

@end

@interface PopOverGenericVC : UITableViewController

@property (nonatomic, weak) id<ListViewPopOverControllerDelegate> popOverDelegate;
@property (nonatomic, strong) NSMutableArray *genericListArray;

-(id)initWithArray:(NSMutableArray*)ListArray andStyle:(UITableViewStyle)style;

@end