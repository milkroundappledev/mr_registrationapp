//
//  AppDelegate.h
//  RegistrationApp
//
//  Created by Afzal Valiji on 25/01/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
