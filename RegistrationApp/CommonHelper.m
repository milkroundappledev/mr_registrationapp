//
//  CommonHelper.m
//  RegistrationApp
//
//  Created by Mounir on 30/01/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "CommonHelper.h"

@implementation CommonHelper


+ (CommonHelper *)GlobalHelper
{
    static CommonHelper *singleton;
    
    @synchronized(self)
    {
        if (!singleton)
            singleton = [[CommonHelper alloc] init];
        
        return singleton;
    }
}

-(void)DisplayMessageBox: (NSString*)Message withMessageTitle:(NSString *)Title andCancelBtnMsg:(NSString*)CancelText;{
}

-(NSString*)DateToString: (NSDate*)Date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    NSString *dateString = [dateFormatter stringFromDate:Date];
    
    return dateString;
}

@end
