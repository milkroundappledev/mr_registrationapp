//
//  LogErrorsHelper.h
//  RegistrationApp
//
//  Created by Rob Taylor on 04/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQLiteManager.h"

@interface LogErrorsHelper : NSObject
+ (void) saveError:(NSString*) errorDescription;
- (NSMutableArray *)loadErrorList;
- (void)clearErrorList;
@end
