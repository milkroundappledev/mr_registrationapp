

//
//  WebserviceUtils.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 14/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "WebserviceUtils.h"


@implementation WebserviceUtils

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (int)registerCandidate:(NSString*)serverName :(CandidateDetails*)candidate :(NSString*)serverId :(NSString*)serverPassword
{
    if (candidate==nil) return false;
    NSDictionary *feed = nil;
    int result = 0;
    @try {

//        NSString *url = [NSString stringWithFormat:@"%@/Services/BulkUploadDataFactory.svc/SaveEarlyCareerRegistration?providerId=%@&providerSecurityToken=%@&FirstName=%@&LastName=%@&schoolLeavingYear=%d&email=%@&courselevel=%d",serverName,serverId,serverPassword,candidate.FirstName,candidate.LastName,candidate.SchoolLeavingYear,candidate.Email,candidate.CourseLevelId];
//
            NSString *url = [NSString stringWithFormat:@"http://stg.milkround.com/Services/BulkUploadDataFactory.svc/SaveEarlyCareerRegistration?providerId=7&providerSecurityToken=a27996fb-2dc3-4e5b-85e5-aba31d6721d6&FirstName=%@&LastName=%@&schoolLeavingYear=%d&email=%@&courselevel=%d",candidate.FirstName,candidate.LastName,candidate.SchoolLeavingYear,candidate.Email,candidate.CourseLevelId];
        
        id response = [self objectWithUrl:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
        feed = (NSDictionary *)response;
        
        // get the array of "stream" from the feed and cast to NSArray
        NSArray *list = (NSArray *)[feed valueForKey:@"d"];
        
        //email address
        NSString *emailAddress;
        
        // loop over all the uni objects
        int loop;
        for (loop = 0; loop<list.count; loop++)
        {
            NSDictionary *registrationItem = (NSDictionary *)[list objectAtIndex:loop];
            emailAddress = [registrationItem valueForKey:@"Key"];
            if ([emailAddress caseInsensitiveCompare:candidate.Email] == NSOrderedSame)
            {
                if ([[registrationItem valueForKey:@"Value"] intValue] != 3) {
                result = [[registrationItem valueForKey:@"Value"] intValue];
                }
            }
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        result = 3;
        return result;
    }
    return result;
}


- (NSString *)stringWithUrl:(NSURL *)url
{
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:600];
    // Fetch the JSON response
	NSData *urlData;
	NSURLResponse *response;
	NSError *error;
    
	// Make synchronous request
	urlData = [NSURLConnection sendSynchronousRequest:urlRequest
                                    returningResponse:&response
                                                error:&error];
    


    if (error) {
        //NSLog(@"%@:%s Error saving context: %@", [self class], _cmd,  [error localizedDescription]);
        [NSException raise:@"TimeOutError" format:@"%@:%s Error: %@", [self class], _cmd,  [error localizedDescription]];
    }
    

    
 	// Construct a String around the Data from the response
	return [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
}

- (id) objectWithUrl:(NSURL *)url
{
    SBJsonParser *parser = [[SBJsonParser alloc]init];
	NSString *jsonString = [self stringWithUrl:url];
    NSDictionary *results = [parser objectWithString:jsonString];
	return results;
}
@end
