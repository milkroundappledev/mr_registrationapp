//
//  CandidateDetailVCell.m
//  RegistrationApp
//
//  Created by Afzal Valiji on 08/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "CandidateDetailVCell.h"

@implementation CandidateDetailVCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(IBAction)EditButtonPressed :(id)sender {
    //send the delegate function with the amount entered by the user
    [_delegate EditCandidateButtonClicked: _rowId];
}

@end
