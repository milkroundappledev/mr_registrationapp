////
////  DataUtils.h
////  Candidate Registrations
////
////  Created by Robert Taylor on 17/07/2011.
////  Copyright 2011 Home. All rights reserved.
////
//
#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "CandidateEvent.h"
#import "WebserviceUtils.h"
#import "KeyChain.h"

@interface DataUtils : NSObject {
}

- (BOOL)checkPassword:(NSString*)password;
- (NSString*)getProviderId;
- (NSString*)getProviderPassword;
- (NSString*)getServerName;
- (BOOL)setServerName :(NSString*)serverName;
- (BOOL)setProviderId :(NSString*)providerId :(NSString*)providerPassword;
- (void)resetPassword:(BOOL)isITAdmin;
- (BOOL)saveNewPassword :(NSString*)oldPassword :(NSString*)newPassword;
@end
