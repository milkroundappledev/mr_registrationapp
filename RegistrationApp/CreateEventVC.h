//
//  CreateEventVC.h
//  RegistrationApp
//
//  Created by Afzal Valiji on 28/01/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CandidateEvent.h"
#import "CandidateEventsHelper.h"
#import "FormatHelper.h"

@protocol CreateEventControllerDelegate <NSObject>

-(void) RefreshTableView ;

@end

@interface CreateEventVC : UIViewController<UIPopoverControllerDelegate>

@property bool IsEditMode;
@property int EventId;

@property (weak, nonatomic) IBOutlet UIButton *uxSelectDateButton;
@property (weak, nonatomic) IBOutlet UITextField *uxEventName;
@property (weak, nonatomic) IBOutlet UITextField *uxEventDate;
@property (weak, nonatomic) IBOutlet UIButton *uxCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *uxSaveButton;

- (IBAction) CancelButtonClicked:(id)sender;
- (IBAction) SaveButtonClicked:(id)sender;
- (IBAction) dateButtonAction:(id)sender;

@property (nonatomic, assign) id<UIPopoverControllerDelegate> delegate;

@property(nonatomic,assign)id <CreateEventControllerDelegate> saveDelegate;

@end

UIDatePicker *datepicker;
UIPopoverController *popOverForDatePicker;


