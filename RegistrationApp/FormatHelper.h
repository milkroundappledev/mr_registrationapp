//
//  FormatHelper.h
//  RegistrationApp
//
//  Created by Afzal Valiji on 04/02/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FormatHelper : NSObject

+ (NSString *) GetStringFromDate:(NSDate *) date;
+ (NSDate *) GetDateFromString:(NSString *) str;
+ (NSString *) GetDBStringFromDate:(NSDate *) date;

@end
