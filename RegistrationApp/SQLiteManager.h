//
//  SQLiteManager.h
//  collections
//
//  Created by Ester Sanchez on 10/03/11.
//  Copyright (c) 2012 MYFALLBACK LTD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
//#import "AppDelegate.h"

enum errorCodes {
	kDBNotExists,
	kDBFailAtOpen, 
	kDBFailAtCreate,
	kDBErrorQuery,
	kDBFailAtClose
};

@interface SQLiteManager : NSObject {

	sqlite3 *db; // The SQLite db reference
	NSString *databaseName; // The database name
}

- (id)initWithDatabaseNamed:(NSString *)name;
- (id)initWithDefaultDatabase;

// SQLite Operations
- (NSError *) openDatabase;
- (NSError *) doQuery:(NSString *)sql;
- (NSError *)doQueryWithoutTran:(NSString *)sql;
- (NSArray *) getRowsForQuery:(NSString *)sql;
- (NSError *) closeDatabase;
- (sqlite3 *) db;

- (NSMutableArray *)getMutableRowsForQuery:(NSString *)sql;
- (NSInteger)SaveImageIntImages:(UIImage *)Image;
- (UIImage *)GetImageFromtImages:(NSInteger)lImageID;
- (NSData *)GetImageNSDatatImages:(NSInteger)lImageID;
- (void)UpdateImageIntImages:(NSInteger)lImageID ImageData:(NSData *)imgData;
- (NSData *)GetUserData:(NSInteger)lUserDataID;
- (void)SaveUserData:(int)intUserDataID Data:(NSData *)Data;

- (int)GetDataCount:(NSString *)sqlQuery;

- (void)UpdateTranID;
 
@end
