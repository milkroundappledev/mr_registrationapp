//
//  ViewController.m
//  RegistrationApp
//
//  Created by Afzal Valiji on 25/01/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CandidateEventsHelper * oppBuilder = [[CandidateEventsHelper alloc] init];
    _EventList = [oppBuilder GetAllCandidateEvents];
    _CreateEventVC.saveDelegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_EventList count];
}

- (EventListTableVCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    EventListTableVCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[EventListTableVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    CandidateEvent * currentEvent = [_EventList objectAtIndex:indexPath.row];
    
    cell.rowId = currentEvent.Id;
    cell.uxEventName.text = currentEvent.EventName;
    cell.uxEventDate.text = [FormatHelper GetStringFromDate:currentEvent.EventDate];
    cell.uxTotalCount.text = [NSString stringWithFormat:@"%d", currentEvent.TotalRegCount];
    cell.uxUploadedCount.text = [NSString stringWithFormat:@"%d", currentEvent.UploadedRegCount];
    cell.uxFailedCount.text =[NSString stringWithFormat:@"%d", currentEvent.FailedRegCount];
    cell.delegate = self;
    return cell;
}

-(void) RefreshTableView
{
    CandidateEventsHelper * oppBuilder = [[CandidateEventsHelper alloc] init];
    _EventList = [oppBuilder GetAllCandidateEvents];
    [self.EventListTable reloadData];
}

-(void) CreateEventButtonClicked:(id)sender
{
    CreateEventVC *destController = [self.storyboard instantiateViewControllerWithIdentifier: @"CreateEvent"];
    destController.saveDelegate = self;
    [self presentViewController:destController animated:NO completion:nil];
}
-(void) EditEventButtonClicked:(NSInteger)rowId
{
    CreateEventVC *destController = [self.storyboard instantiateViewControllerWithIdentifier: @"CreateEvent"];
    destController.EventId = rowId;
    destController.saveDelegate = self;
   [self presentViewController:destController animated:true completion:nil];
}

-(void) ViewEventDetailsButtonClicked:(NSInteger)rowId
{
    ViewEventDetailsVC *destController = [self.storyboard instantiateViewControllerWithIdentifier: @"ViewEventDetails"];
    destController.EventId = rowId;
    [self presentViewController:destController animated:true completion:nil];
}
-(void) RegisterButtonClicked:(NSInteger)rowId
{
    RegistrationVC *destController = [self.storyboard instantiateViewControllerWithIdentifier: @"Registration"];
    destController.EventId = rowId;
    [self presentViewController:destController animated:true completion:nil];

}
- (void) UploadButtonClicked:(NSInteger)rowId
{
    
    MessageOverlayVC *messageController = [self.storyboard instantiateViewControllerWithIdentifier: @"MessageOverlay"];

    messageController.ShowCloseButton = FALSE;
    messageController.Title = @"Uploading...";
    messageController.Message = @"Please wait whilst the candidates are uploaded.";
    self.MessageOverlay = messageController;
    [self.view addSubview:_MessageOverlay.view];
    _UploadEventId = rowId;
    [NSThread detachNewThreadSelector:@selector(startDataUpload) toTarget:self withObject:nil];
}

-(void) startDataUpload
{
    
    @autoreleasepool {
        
        RegistrationHelper *uploadHelper = [[RegistrationHelper alloc] init];
        _IsDataUploadSuccessful = [uploadHelper uploadCandidates:_UploadEventId];
        
        [self performSelectorOnMainThread:@selector(dataUploadComplete) withObject:nil waitUntilDone:NO];
    }
    
    
}
-(void) dataUploadComplete
{
    // close this view
    [self.MessageOverlay CloseOverlay];
    
    if (_IsDataUploadSuccessful==FALSE)
    {
        MessageOverlayVC *messageController = [self.storyboard instantiateViewControllerWithIdentifier: @"MessageOverlay"];
        messageController.ShowCloseButton = TRUE;
        messageController.Title = @"Oops!";
        messageController.Message = @"Data sync failed";
        self.MessageOverlay = messageController;
        [self.view addSubview:_MessageOverlay.view];
    }
    else
    {
        MessageOverlayVC *messageController = [self.storyboard instantiateViewControllerWithIdentifier: @"MessageOverlay"];

        messageController.ShowCloseButton = TRUE;
        messageController.Title = @"Success!";
        messageController.Message = @"Data sync was successful";
        self.MessageOverlay = messageController;
        [self.view addSubview:_MessageOverlay.view];
    }
    _IsDataUploadSuccessful = FALSE;
    [self RefreshTableView];
}

- (IBAction)ViewSettingsClicked:(id)sender
{
    UIViewController *destinationViewController = [self.storyboard instantiateViewControllerWithIdentifier: @"Settings"];
    [self presentViewController:destinationViewController animated:true completion:nil];
}

- (IBAction)ViewErrorLogClicked:(id)sender
{
    UIViewController *destinationViewController = [self.storyboard instantiateViewControllerWithIdentifier: @"ErrorLogList"];
    [self presentViewController:destinationViewController animated:true completion:nil];
}

@end
