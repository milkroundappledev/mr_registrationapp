//
//  ViewController.h
//  RegistrationApp
//
//  Created by Afzal Valiji on 25/01/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewEventDetailsVC.h"
#import "RegistrationVC.h"
#import "EventListTableVCell.h"
#import "WebserviceUtils.h"
#import "RegistrationHelper.h"
#import "MessageOverlayVC.h"

@interface ViewController : UIViewController <EventListTableCellDelegate,CreateEventControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *CreateEventButton;
@property (weak, nonatomic) IBOutlet UITableView *EventListTable;
@property (strong, nonatomic) NSArray *EventList;

@property int UploadEventId;
@property bool IsDataUploadSuccessful;
@property (nonatomic, strong) CreateEventVC *CreateEventVC;
@property (strong, nonatomic) MessageOverlayVC *MessageOverlay;

- (IBAction)CreateEventButtonClicked:(id)sender;
@end
