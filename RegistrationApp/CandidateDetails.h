//
//  CandidateDetails.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 09/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CandidateDetails : NSObject

@property(readwrite,assign) int rowId;
@property(readwrite,assign) int EventId;
@property(readwrite,assign) int CourseLevelId;
@property(readwrite,assign) int UploadStatus;
@property(readwrite,assign) int SchoolLeavingYear;
@property(nonatomic,copy) NSString *FirstName;
@property(nonatomic,copy) NSString *LastName;
@property(nonatomic,copy) NSString *Email;
@property(nonatomic,copy) NSString *CourseName;
@property(nonatomic,copy) NSDate *DateCreated;
@property(nonatomic,copy) NSDate *UploadDate;


//CREATE TABLE Registrations (
//                            Id integer PRIMARY KEY NOT NULL,
//                            EventId integer NOT NULL,
//                            FirstName varchar NOT NULL,
//                            LastName varchar NOT NULL,
//                            Email varchar NOT NULL,
//                            DateCreated datetime NOT NULL DEFAULT(CURRENT_TIMESTAMP),
//                            UploadStatus integer NOT NULL DEFAULT(0),
//                            SchoolLeavingYear integer DEFAULT(null),
//                            UploadDate datetime,
//                            CourseLevelId integer
//                            );

@end