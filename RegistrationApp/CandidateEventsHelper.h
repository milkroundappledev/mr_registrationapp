//
//  CandidateEventsHelper.h
//  RegistrationApp
//
//  Created by Afzal Valiji on 30/01/2014.
//  Copyright (c) 2014 Milkround. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CandidateEvent.h"

@interface CandidateEventsHelper : NSObject

-(bool) EditCandidateEvent: (CandidateEvent *)event;
-(bool) CreateCandidateEvent: (CandidateEvent *)event;
-(NSArray *) GetAllCandidateEvents;
-(CandidateEvent *) GetCandiateEventById:(int) identifier;

@end
