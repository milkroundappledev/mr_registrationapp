//
//  ErrorLogItem.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 13/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorLogItem : NSObject
{
    int rowId;
    NSString *errorDescription;
    NSDate *errorDate;
}

@property(nonatomic,copy) NSString *errorDescription;
@property(nonatomic,copy) NSDate *errorDate;
@property(readwrite,assign) int rowId;

@end
