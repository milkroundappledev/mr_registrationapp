//
//  SQLiteManager.m
//  collections
//
//  Created by Ester Sanchez on 10/03/11.
//  Copyright (c) 2012 MYFALLBACK LTD. All rights reserved.
//
// Test Chages

#import "SQLiteManager.h"

// Private methods
@interface SQLiteManager (Private)

- (NSString *)getDatabasePath;
- (NSError *)createDBErrorWithDescription:(NSString*)description andCode:(int)code;

@end



@implementation SQLiteManager

#pragma mark Init & Dealloc

/**
 * Init method. 
 * Use this method to initialise the object, instead of just "init".
 * 
 * @param name the name of the database to manage.
 *
 * @return the SQLiteManager object initialised.
 */

- (id) initWithDatabaseNamed:(NSString *)name; {
	self = [super init];
	if (self != nil) {
		databaseName = [[NSString alloc] initWithString:name];
		db = nil;
	}
	return self;
}

- (id) initWithDefaultDatabase {
    self = [super init];
	if (self != nil) {
        
		databaseName = @"RegistrationDB.sqlite";
		db = nil;
	}
	return self;
}

/**
 * Dealloc method.
 */

- (void)dealloc {

	if (db != nil) {
		[self closeDatabase];
	}

}

#pragma mark SQLite Operations


/**
 * Open or create a SQLite3 database.
 *
 * If the db exists, then is opened and ready to use. If not exists then is created and opened.
 *
 * @return nil if everything was ok, an NSError in other case.
 *
 */
- (sqlite3 *) db {
    return db;
}
- (NSError *) openDatabase {
	
	NSError *error = nil;
	
	NSString *databasePath = [self getDatabasePath];
		
	const char *dbpath = [databasePath UTF8String];
	int result = sqlite3_open(dbpath, &db);
	if (result != SQLITE_OK) {
			const char *errorMsg = sqlite3_errmsg(db);
			NSString *errorStr = [NSString stringWithFormat:@"The database could not be opened: %@",[NSString stringWithCString:errorMsg encoding:NSUTF8StringEncoding]];
			error = [self createDBErrorWithDescription:errorStr	andCode:kDBFailAtOpen];
	}
	
	return error;
}


/**
 * Does an SQL query. 
 *
 * You should use this method for everything but SELECT statements.
 *
 * @param sql the sql statement.
 *
 * @return nil if everything was ok, NSError in other case.
 */


- (NSError *)doQuery:(NSString *)sql {
	
	NSError *openError = nil;
	NSError *errorQuery = nil;
	
	//Check if database is open and ready.
	if (db == nil) {
		openError = [self openDatabase];
	}
	
	if (openError == nil) {		
		sqlite3_stmt *statement;	
		const char *query = [sql UTF8String];
		sqlite3_prepare_v2(db, query, -1, &statement, NULL);
		
		if (sqlite3_step(statement) == SQLITE_ERROR) {
			const char *errorMsg = sqlite3_errmsg(db);
			errorQuery = [self createDBErrorWithDescription:[NSString stringWithCString:errorMsg encoding:NSUTF8StringEncoding]
													andCode:kDBErrorQuery];
		}
		sqlite3_finalize(statement);
		[self closeDatabase];
	}
	else {
		errorQuery = openError;
	}

    [self UpdateTranID];
	return errorQuery;
}
- (NSError *)doQueryWithoutTran:(NSString *)sql {
	
	NSError *openError = nil;
	NSError *errorQuery = nil;
	
	//Check if database is open and ready.
	if (db == nil) {
		openError = [self openDatabase];
	}
	
	if (openError == nil) {
		sqlite3_stmt *statement;
		const char *query = [sql UTF8String];
		sqlite3_prepare_v2(db, query, -1, &statement, NULL);
		
		if (sqlite3_step(statement) == SQLITE_ERROR) {
			const char *errorMsg = sqlite3_errmsg(db);
			errorQuery = [self createDBErrorWithDescription:[NSString stringWithCString:errorMsg encoding:NSUTF8StringEncoding]
													andCode:kDBErrorQuery];
		}
		sqlite3_finalize(statement);
		[self closeDatabase];
	}
	else {
		errorQuery = openError;
	}
    
	return errorQuery;
}
/**
 * Does a SELECT query and gets the info from the db.
 *
 * The return array contains an NSDictionary for row, made as: key=columName value= columnValue.
 *
 * For example, if we have a table named "users" containing:
 * name | pass
 * -------------
 * admin| 1234
 * pepe | 5678
 * 
 * it will return an array with 2 objects:
 * resultingArray[0] = name=admin, pass=1234;
 * resultingArray[1] = name=pepe, pass=5678;
 * 
 * So to get the admin password:
 * [[resultingArray objectAtIndex:0] objectForKey:@"pass"];
 *
 * @param sql the sql query (remember to use only a SELECT statement!).
 * 
 * @return an array containing the rows fetched.
 */

- (NSMutableArray *)getMutableRowsForQuery:(NSString *)sql {
	
	NSMutableArray *resultsArray = [[NSMutableArray alloc] initWithCapacity:1];
	
	if (db == nil) {
		[self openDatabase];
	}
	
	sqlite3_stmt *statement;	
	const char *query = [sql UTF8String];
	int returnCode = sqlite3_prepare_v2(db, query, -1, &statement, NULL);
	
	if (returnCode == SQLITE_ERROR) {
		const char *errorMsg = sqlite3_errmsg(db);
		NSError *errorQuery = [self createDBErrorWithDescription:[NSString stringWithCString:errorMsg encoding:NSUTF8StringEncoding]
                                                         andCode:kDBErrorQuery];
		NSLog(@"%@", errorQuery);
	}
	
	while (sqlite3_step(statement) == SQLITE_ROW) {
		int columns = sqlite3_column_count(statement);
		NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithCapacity:columns];
        
		for (int i = 0; i<columns; i++) {
			const char *name = sqlite3_column_name(statement, i);	
            
			NSString *columnName = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
			
			int type = sqlite3_column_type(statement, i);
			
			switch (type) {
				case SQLITE_INTEGER:
				{
					int value = sqlite3_column_int(statement, i);
					[result setObject:[NSNumber numberWithInt:value] forKey:columnName];
					break;
				}
				case SQLITE_FLOAT:
				{
					float value = sqlite3_column_int(statement, i);
					[result setObject:[NSNumber numberWithFloat:value] forKey:columnName];
					break;
				}
				case SQLITE_TEXT:
				{
					const char *value = (const char*)sqlite3_column_text(statement, i);
					[result setObject:[NSString stringWithCString:value encoding:NSUTF8StringEncoding] forKey:columnName];
					break;
				}
                    
				case SQLITE_BLOB:
					break;
				case SQLITE_NULL:
					[result setObject:[NSNull null] forKey:columnName];
					break;
                    
				default:
				{
					const char *value = (const char *)sqlite3_column_text(statement, i);
					[result setObject:[NSString stringWithCString:value encoding:NSUTF8StringEncoding] forKey:columnName];
					break;
				}
                    
			} //end switch
			
			
		} //end for
		
		[resultsArray addObject:result];
        
	} //end while
	sqlite3_finalize(statement);
	
	[self closeDatabase];
	
	return resultsArray;
    
}
- (NSArray *)getRowsForQuery:(NSString *)sql {
	
	NSMutableArray *resultsArray = [[NSMutableArray alloc] initWithCapacity:1];
	
	if (db == nil) {
		[self openDatabase];
	}
	
	sqlite3_stmt *statement;	
	const char *query = [sql UTF8String];
	int returnCode = sqlite3_prepare_v2(db, query, -1, &statement, NULL);
	
	if (returnCode == SQLITE_ERROR) {
		const char *errorMsg = sqlite3_errmsg(db);
		NSError *errorQuery = [self createDBErrorWithDescription:[NSString stringWithCString:errorMsg encoding:NSUTF8StringEncoding]
												andCode:kDBErrorQuery];
		NSLog(@"%@", errorQuery);
	}
	
	while (sqlite3_step(statement) == SQLITE_ROW) {
		int columns = sqlite3_column_count(statement);
		NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithCapacity:columns];

		for (int i = 0; i<columns; i++) {
			const char *name = sqlite3_column_name(statement, i);	

			NSString *columnName = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
			
			int type = sqlite3_column_type(statement, i);
			
			switch (type) {
				case SQLITE_INTEGER:
				{
					int value = sqlite3_column_int(statement, i);
					[result setObject:[NSNumber numberWithInt:value] forKey:columnName];
					break;
				}
				case SQLITE_FLOAT:
				{
					float value = sqlite3_column_int(statement, i);
					[result setObject:[NSNumber numberWithFloat:value] forKey:columnName];
					break;
				}
				case SQLITE_TEXT:
				{
					const char *value = (const char*)sqlite3_column_text(statement, i);
					[result setObject:[NSString stringWithCString:value encoding:NSUTF8StringEncoding] forKey:columnName];
					break;
				}
				
				case SQLITE_BLOB:
					break;
				case SQLITE_NULL:
					[result setObject:[NSNull null] forKey:columnName];
					break;

				default:
				{
					const char *value = (const char *)sqlite3_column_text(statement, i);
					[result setObject:[NSString stringWithCString:value encoding:NSUTF8StringEncoding] forKey:columnName];
					break;
				}

			} //end switch
			
			
		} //end for
		
		[resultsArray addObject:result];

	} //end while
	sqlite3_finalize(statement);
	
	[self closeDatabase];
	
	return resultsArray;

}
- (UIImage *)GetImageFromtImages:(NSInteger)lImageID {    
    NSError *openError = nil;
    NSData *imgData = nil;
    
	if (db == nil) {
		openError = [self openDatabase];
	}
	
	if (openError == nil) {	
        const char *query = "SELECT bImage FROM tImages WHERE lImageID = ?";
        sqlite3_stmt *statement = nil;
        
        sqlite3_prepare_v2(db, query, -1, &statement, NULL);
        
        sqlite3_bind_int(statement, 1, lImageID);
        
        if (sqlite3_step(statement) == SQLITE_ROW){
            imgData = [[NSData alloc] initWithBytes:sqlite3_column_blob(statement,0) length:sqlite3_column_bytes(statement, 0)];
        }
        
        sqlite3_reset(statement);
        sqlite3_finalize(statement);
        [self closeDatabase];
    }
    
    UIImage *retVal = [[UIImage alloc] initWithData:imgData] ;

    return retVal;
}

- (NSInteger)SaveImageIntImages:(UIImage *)Image {
    
    NSError *openError = nil;
    NSInteger intRetVal = -1;
    
	//Check if database is open and ready.
	if (db == nil) {
		openError = [self openDatabase];
	}
	
	if (openError == nil) {	
        NSData *imgData = UIImagePNGRepresentation(Image);
        const char *query = "INSERT INTO tImages (bImage) VALUES (?)";
        sqlite3_stmt *statement = nil;
    
        sqlite3_prepare_v2(db, query, -1, &statement, NULL);
    

        sqlite3_bind_blob(statement, 1, [imgData bytes], [imgData length], NULL);
    
        if (SQLITE_DONE != sqlite3_step(statement))
            NSAssert(0,@"Error while trying to save image. '%s'", sqlite3_errmsg(db));
    
        sqlite3_reset(statement);
        [self closeDatabase];
        
        NSArray *tImageID = [self getRowsForQuery:@"SELECT lImageID FROM tImages ORDER BY lImageID DESC LIMIT 1"];
        NSString *strlImageID = [[tImageID objectAtIndex:0] objectForKey:@"lImageID"];
        intRetVal = [strlImageID integerValue];
    }
    [self UpdateTranID];
    return intRetVal;
}
- (NSData *)GetImageNSDatatImages:(NSInteger)lImageID {
    NSError *openError = nil;
    NSData *imgData = nil;
    
	if (db == nil) {
		openError = [self openDatabase];
	}
	
	if (openError == nil) {
        const char *query = "SELECT bImage FROM tImages WHERE lImageID = ?";
        sqlite3_stmt *statement = nil;
        
        sqlite3_prepare_v2(db, query, -1, &statement, NULL);
        
        sqlite3_bind_int(statement, 1, lImageID);
        
        if (sqlite3_step(statement) == SQLITE_ROW){
            imgData = [[NSData alloc] initWithBytes:sqlite3_column_blob(statement,0) length:sqlite3_column_bytes(statement, 0)];
        }
        
        sqlite3_reset(statement);
        sqlite3_finalize(statement);
        [self closeDatabase];
    }
    
    return imgData;
}
- (void)UpdateTranID {
    NSArray *tWorkingSettings = [self getRowsForQuery:@"SELECT * FROM tSettings WHERE cName='DBTranID'"];
    if ([tWorkingSettings count] == 0)
        return;
    double dblWorkingTranID = [[[tWorkingSettings objectAtIndex:0] objectForKey:@"cValue"] doubleValue];
    dblWorkingTranID++;
    [self doQueryWithoutTran:[NSString stringWithFormat:@"UPDATE tSettings SET cValue='%f' WHERE cName='DBTranID'",dblWorkingTranID]];
}
- (void)UpdateImageIntImages:(NSInteger)lImageID ImageData:(NSData *)imgData {
    
    NSError *openError = nil;
    
    if ([imgData length] == 0)
        return;
    
	//Check if database is open and ready.
	if (db == nil) {
		openError = [self openDatabase];
	}
	
	if (openError == nil) {
        const char *query = "UPDATE tImages SET bImage=? WHERE lImageID=?";
        sqlite3_stmt *statement = nil;
        
        sqlite3_prepare_v2(db, query, -1, &statement, NULL);
        
        
        sqlite3_bind_blob(statement, 1, [imgData bytes], [imgData length], NULL);
        sqlite3_bind_int(statement, 2, lImageID);
        
        if (SQLITE_DONE != sqlite3_step(statement))
            NSAssert(0,@"Error while trying to save image. '%s'", sqlite3_errmsg(db));
        
        sqlite3_reset(statement);
        [self closeDatabase];
    }
}
- (NSData *)GetUserData:(NSInteger)lUserDataID {    
    NSError *openError = nil;
    NSData *Data = nil;
    
	if (db == nil) {
		openError = [self openDatabase];
	}
	
	if (openError == nil) {	
        const char *query = "SELECT bUserData FROM tUserData WHERE lUserDataID = ?";
        sqlite3_stmt *statement = nil;
        
        sqlite3_prepare_v2(db, query, -1, &statement, NULL);
        
        sqlite3_bind_int(statement, 1, lUserDataID);
        
        if (sqlite3_step(statement) == SQLITE_ROW){
            Data = [[NSData alloc] initWithBytes:sqlite3_column_blob(statement,0) length:sqlite3_column_bytes(statement, 0)];
        }
        
        sqlite3_reset(statement);
        sqlite3_finalize(statement);
        [self closeDatabase];
    }
       
    return Data;
}

- (int)GetDataCount:(NSString *)sqlQuery {
    NSError *openError = nil;
    int Data = 0;
    
	if (db == nil) {
		openError = [self openDatabase];
	}
	
	if (openError == nil) {
        const char *query =[sqlQuery UTF8String];
        sqlite3_stmt *statement = nil;
        
        sqlite3_prepare_v2(db, query, -1, &statement, NULL);
        
        //sqlite3_bind_int(statement, 1, lUserDataID);
        
        if (sqlite3_step(statement) == SQLITE_ROW){
            Data = sqlite3_column_int(statement, 0);
        }
        
        sqlite3_reset(statement);
        sqlite3_finalize(statement);
        [self closeDatabase];
    }
    
    return Data;
}

- (void)SaveUserData:(int)intUserDataID Data:(NSData *)Data {
    
    NSError *openError = nil;
    
	//Check if database is open and ready.
	if (db == nil) {
		openError = [self openDatabase];
	}
	
	if (openError == nil) {	
       
        
        const char *query = "UPDATE tUserData set bUserData = ? WHERE lUserDataID = ?";
        sqlite3_stmt *statement = nil;
        
        sqlite3_prepare_v2(db, query, -1, &statement, NULL);
        

        sqlite3_bind_blob(statement, 1, [Data bytes], [Data length], NULL);
        sqlite3_bind_int(statement, 2, intUserDataID);
        
        if (SQLITE_DONE != sqlite3_step(statement))
            NSAssert(0,@"Error while trying to save user data. '%s'", sqlite3_errmsg(db));
        
        sqlite3_reset(statement);
        [self closeDatabase];
    }
    [self UpdateTranID];
}

/**
 * Closes the database.
 *
 * @return nil if everything was ok, NSError in other case.
 */

- (NSError *) closeDatabase {
	
	NSError *error = nil;
	
	
	if (db != nil) {
		if (sqlite3_close(db) != SQLITE_OK){
			const char *errorMsg = sqlite3_errmsg(db);
			NSString *errorStr = [NSString stringWithFormat:@"The database could not be closed: %@",[NSString stringWithCString:errorMsg encoding:NSUTF8StringEncoding]];
			error = [self createDBErrorWithDescription:errorStr andCode:kDBFailAtClose];
		}
		
		db = nil;
	}
	
	return error;
}



@end


#pragma mark -
@implementation SQLiteManager (Private)

/**
 * Gets the database file path (in NSDocumentDirectory).
 *
 * @return the path to the db file.
 */

- (NSString *)getDatabasePath{
	
	if([[NSFileManager defaultManager] fileExistsAtPath:databaseName]){
		// Already Full Path
		return databaseName;
	} else {
	// Get the documents directory
	NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *strRetVal = [docsDir stringByAppendingPathComponent:databaseName];
        
    if ([[NSFileManager defaultManager] fileExistsAtPath:strRetVal])
    	return strRetVal;

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *appSupportDir = [paths objectAtIndex:0];
    strRetVal = [appSupportDir stringByAppendingPathComponent:databaseName];

    if ([[NSFileManager defaultManager] fileExistsAtPath:strRetVal])
        return strRetVal;
    }
    return  databaseName;
}

/**
 * Creates an NSError.
 *
 * @param description the description wich can be queried with [error localizedDescription];
 * @param code the error code (code erors are defined as enum in the header file).
 *
 * @return the NSError just created.
 *
 */
- (NSError *)createDBErrorWithDescription:(NSString*)description andCode:(int)code {
	
	NSDictionary *userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:description, NSLocalizedDescriptionKey, nil];
	NSError *error = [NSError errorWithDomain:@"SQLite Error" code:code userInfo:userInfo];
	
	return error;
}


@end

